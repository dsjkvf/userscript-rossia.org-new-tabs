// ==UserScript==
// @name        rossia.org - open links in new tabs
// @namespace   oyvey
// @homepage    https://bitbucket.org/dsjkvf/userscript-rossia.org-new-tabs
// @downloadURL https://bitbucket.org/dsjkvf/userscript-rossia.org-new-tabs/raw/master/ront.user.js
// @updateURL   https://bitbucket.org/dsjkvf/userscript-rossia.org-new-tabs/raw/master/ront.user.js
// @match       *://*.rossia.org/*
// @run-at      document-end
// @grant       none
// @version     1.0.2
// ==/UserScript==

var links = document.getElementsByTagName('a');
for (var i = 0; i < links.length; i++) {
    if (links[i].href.indexOf("tiphareth") == -1) {
        links[i].target = '_blank';
    }
}
