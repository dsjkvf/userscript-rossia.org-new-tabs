LJR: open links in new tabs
===========================

## About

This is a small [userscript](https://github.com/OpenUserJs/OpenUserJS.org/wiki/Userscript-beginners-HOWTO) for [Greasemonkey](https://addons.mozilla.org/firefox/addon/greasemonkey/) or similar extension, which -- upon visiting [LJR](http://lj.rossia.org/) pages -- will open links in new tabs.
